import json
import random


# --------------------------------------------------------#
#
# Team: TeamRocket
# Main Authors: Christian Derr + Eric Assmann
#
# --------------------------------------------------------#

class SceneLoader:
    bg = None
    info_txt = None
    cmds = {}
    tmpPosCmd = []
    nextScene = None

    def loadScene(self, scene_id):

        with open('data.json', encoding="utf-8") as jsn_file:
            game_jsn = json.load(jsn_file)

        if scene_id in game_jsn:
            game_jsn = game_jsn[scene_id]

            self.bg = game_jsn["bg"]
            self.info_txt = game_jsn["info_txt"]

            tmpCmds = game_jsn["cmds"]
            self.nextScene = None
            self.tmpPosCmd = []
            for cmd in tmpCmds.keys():
                tmpSt = {"depend": None, "txt": None, "active": None, "addr": None}

                tmpCmd = tmpCmds[cmd]

                tmpSt["depend"] = tmpCmd[0]
                tmpSt["txt"] = tmpCmd[1]
                if tmpCmd[2] == 0:
                    tmpSt["active"] = False
                else:
                    tmpSt["active"] = True
                tmpSt["addr"] = tmpCmd[3]

                self.tmpPosCmd.append(cmd)

                self.cmds[cmd] = tmpSt

    def getPosCmd(self):
        tmpCmds = []
        for cmd in self.tmpPosCmd:
            if cmd[0] == ',' or cmd[1] == ",":
                pass
            else:
                tmpCmd = self.cmds[cmd]
                if tmpCmd["active"] == False or tmpCmd["active"] == None:
                    if len(tmpCmd["depend"]) > 0:
                        allDepDone = True
                        for depend in tmpCmd["depend"]:
                            if self.cmds[depend]["active"] == False:
                                allDepDone = False

                        if allDepDone:
                            tmpCmds.append(cmd)
                    else:
                        tmpCmds.append(cmd)

        return tmpCmds

    def toggleActive(self, cmd):
        tmpCmd = self.cmds[cmd]

        if not cmd[0] == '.':
            self.cmds[cmd]["active"] = not tmpCmd["active"]

            for addr in tmpCmd["addr"]:
                if addr[0] == '$':
                    addr = addr[1:]
                    if random.randint(0, 201) > 100:
                        self.cmds[addr]["active"] = not self.cmds[addr]["active"]
                else:
                    self.cmds[addr]["active"] = not self.cmds[addr]["active"]
        else:
            self.nextScene = self.cmds[cmd]["addr"][0]

        tmpTxt = tmpCmd["txt"]

        for addr in tmpCmd["addr"]:
            if addr[0] == ',' or addr[1] == ',':
                if addr[0] == '$':
                    addr = addr[1:]
                tmpTxt += "\n" + self.getHiddenTxt(self.cmds[addr])
        return tmpTxt

    def getHiddenTxt(self, cmd):
        posPipe = cmd["txt"].find('|') + 1

        # 0 = False = erste Text
        if not cmd["active"]:
            return cmd["txt"][posPipe:]
        else:
            return cmd["txt"][:posPipe - 1]


