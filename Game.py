import tkinter as Tk
import tkfontchooser
from sceneLoader import SceneLoader

# --------------------------------------------------------#
#
# Team: TeamRocket
# Main Authors: Max Tröger
#
# --------------------------------------------------------#

class GameGraphics:
    font_size = 40 #pixels high
    selection = None

    def __init__(self):
        self.root = Tk.Tk()
        self.canvas = Tk.Canvas(self.root)
        self.root.wm_geometry("1100x800")
        self.root.title('LNDC Game Team Rocket')
        self.root.resizable(False, False)
        self.canvas.pack(fill=Tk.BOTH, expand=1)  # Stretch canvas to root window size.


    def Nothing(self, event):
        pass


    def EndThisScene(self, event):
        self.root.quit()
        if self.cmds != None:
            self.selection = self.cmds[self.cmd_pointer]


    def Up(self, event):
        self.move_cmds_pointer(-1)


    def Down(self, event):
        self.move_cmds_pointer(1)


    def move_cmds_pointer(self, movement):
        old_pointer = self.cmd_pointer
        self.cmd_pointer+=movement
        if self.cmd_pointer < 0: # pointer logic
            self.cmd_pointer = len(self.cmds)-1
        elif self.cmd_pointer >= len(self.cmds):
            self.cmd_pointer = 0
        self.canvas.move(self.cmd_pointer_id,0,(self.cmd_pointer-old_pointer)*(self.font_size+4.5)) #4.5 px between two lines


    def Space(self, event): #Print next string
        if len(self.info_text_array)>0:
            self.counter=0
            self.info_text = self.info_text_array.pop(0)
            if self.info_text[0:1] == '*':  # Check for feelings
                self.canvas.itemconfig(self.info_text_id, fill="#43F243")
            else:
                self.canvas.itemconfig(self.info_text_id, fill="white")
            if self.info_text[0:1]==':': #Check for names
                self.info_text = self.info_text[1:]
                name = ""
                for x in range(0,len(self.info_text)-1):
                    if self.info_text[x] != ':':
                        name += self.info_text[x]
                    else:
                        self.info_text = self.info_text[x+1:]
                        break
                self.canvas.itemconfig(self.info_name_id, text=name)
                self.canvas.itemconfig(self.namebox_id, image=self.image_namebox)
            else:
                self.canvas.itemconfig(self.info_name_id, text="")
                self.canvas.itemconfig(self.namebox_id, image="")
            self.draw_info_text()
        else:
            if self.cmds != None:
                self.stopTextWriting = True
                self.canvas.itemconfig(self.cmd_pointer_id, fill="white")
                self.canvas.itemconfig(self.cmds_text_id, text=self.cmds_text)
                self.canvas.itemconfig(self.info_text_id, text="")
                self.canvas.bind("<Return>", self.EndThisScene)
                self.canvas.bind("<Up>", self.Up)
                self.canvas.bind("<Down>", self.Down)
                self.canvas.bind("<space>", self.Nothing)
                self.canvas.itemconfig(self.img_enter_id, image = self.image_enter)
                #self.canvas.itemconfig(self.img_leertaste_id, image = None)
                self.canvas.delete(self.img_leertaste_id)
            else:
                self.EndThisScene(None)


    # Info Text is the big text box at the bottom
    def draw_info_text(self):
        self.canvas.itemconfig(self.info_text_id, text=self.info_text[:self.counter] if not self.stopTextWriting else "") #little if
        self.counter+=1
        if(self.counter > len(self.info_text) or self.stopTextWriting): #stops the endless loop
            return
        self.root.after(8, self.draw_info_text)


    def newScene(self, info_text, bg, cmds): #critical: will be called several times in the game
        self.cmd_pointer = 0 #initialize/reset variables

        self.canvas.delete("all") #deletes all earlier created objects to save storage
        info_text = info_text.replace("<user>", "Bernd")
        self.info_text_array = info_text.split("\n")
        for x in range(0, len(self.info_text_array)-1):
            if x == len(self.info_text_array):
                break
            else:
                if self.info_text_array[x] == '':
                    self.info_text_array.pop(x)


        self.cmds = cmds
        if cmds != None:
            self.cmds_text = "" #initializes the cmds string
            for s in cmds:
                self.cmds_text+=(s if s[0] != '.' else s[1:]) + "\n"

        image_background = Tk.PhotoImage(file=bg) #loads images
        image_textbox = Tk.PhotoImage(file="textbox.png")
        self.image_enter = Tk.PhotoImage(file="enter.png")
        self.image_leertaste = Tk.PhotoImage(file="leertaste.png")
        self.image_namebox = Tk.PhotoImage(file="namebox.png") #will be set as image later when needed

        self.canvas.create_image(0, 0, anchor=Tk.NW, image=image_background)
        self.canvas.create_image(40, 600, anchor=Tk.NW, image=image_textbox)
        self.img_enter_id = self.canvas.create_image(990, 510, anchor=Tk.NW)
        self.img_leertaste_id = self.canvas.create_image(850, 560, anchor=Tk.NW, image = self.image_leertaste)
        self.namebox_id = self.canvas.create_image(40, 515, anchor=Tk.NW)

        font = tkfontchooser.Font(family="Arial", size=-self.font_size)
        self.cmds_text_id = self.canvas.create_text(95, 612, font=font, fill="white", anchor=Tk.NW, text="")
        self.cmd_pointer_id = self.canvas.create_polygon(55,615+self.font_size*0.1,55,615+self.font_size*0.9,55+30,615+self.font_size*0.5, fill="")
        self.info_text_id = self.canvas.create_text(55, 612, font=font, fill="white", anchor=Tk.NW, width=990)
        self.info_name_id = self.canvas.create_text(55, 527, font=font, fill="white", anchor=Tk.NW)

        self.canvas.bind("<Return>", self.Nothing)
        self.canvas.bind("<Up>", self.Nothing)
        self.canvas.bind("<Down>", self.Nothing)
        self.canvas.bind("<space>", self.Space)
        self.stopTextWriting = False
        self.Space(None)
        self.canvas.focus_set()
        self.root.mainloop()


if __name__ == "__main__":
    gg = GameGraphics()
    sl = SceneLoader()
    sl.loadScene("entry")
    gg.newScene(sl.info_txt, sl.bg, sl.getPosCmd())
    while(True):

        txt = sl.toggleActive(gg.selection)

        if gg.selection[0] == '.':
            if not sl.nextScene == "end":
                gg.newScene(txt, sl.bg, None)
                sl.loadScene(sl.nextScene)
                gg.newScene(sl.info_txt, sl.bg, sl.getPosCmd())
            else:
                break
        else:
            gg.newScene(txt, sl.bg, sl.getPosCmd())