# Description
This repository contains a universal concept for a visual novel, developed as part of the "LNDC 2018" Hackathon.
The project was built from scratch within 24h hours of straight coding at the "Westsächsiche Hochschule Zwickau" by a team of 4 Python Developers, 2 Artists and 2 Storywriters.

The Game.py reads a "Storyfile" in form of a .json file and from this creates this kinda-visual-novel.
Skip text and make choices by pressing Spacebar or enter.

Although it still might be necessary to install the "tkinter" python module in order to play this game on Linux.

New story elements or pictures can be added without any problem, just create a new json file or edit the current one (commented version might be added later); in addition you will need to change the .py to your needs.

_The uploaded story is written in German; A few new chapters might be added later aswell_